QT += core gui

CONFIG += c++11

TARGET = BlackPainter-Offline
CONFIG += console
CONFIG -= app_bundle

CONFIG += static

TEMPLATE = app

SOURCES += main.cpp \
    pid_lib/pidtools.cpp \
    pid_lib/metadata.cpp

HEADERS += \
    pid_lib/pidtools.h \
    pid_lib/metadata.h \
    pid_lib/multi_vector.h
